import React from 'react';
import './Ingredients.css';

const Ingredient = props => {
    return (
        <div className='ingredient' id = {props.id}>
            <img src={props.image} alt='ingredients' onClick={props.onAddCount}/>
                <span>{props.name}</span>
                <span>x {props.count}</span>
                <button onClick={props.onDeleteCount}>X</button>
        </div>
    );
};

export default Ingredient;