import Ingredient from "./components/Ingredient/Ingredient";
import {useState} from "react";
import meatImage from './assets/meat.jpg';
import saladImage from './assets/salad.jpg';
import cheeseImage from './assets/cheese.jpg';
import baconImage from './assets/bacon.jpg';
import './App.css';

const App = () => {
    const INGREDIENTS = [
        {name: 'Meat', price: 50, image: meatImage, id:1},
        {name: 'Salad', price: 5, image: saladImage,  id:2},
        {name: 'Cheese', price: 20, image: cheeseImage, id:3},
        {name: 'Bacon', price: 30, image: baconImage, id:4},
    ]

    const [Ingredients, setIngredients] = useState([
        {name: 'Meat',count: 0, id: 1},
        {name: 'Salad',count: 0, id: 2},
        {name: 'Cheese',count: 0,id:3},
        {name: 'Bacon',count: 0, id:4},
    ])

    const totalIngredients = Ingredients.map((ingredient,i)=>{
        return ingredient.count*INGREDIENTS[i].price;
    })

    console.log(totalIngredients)
    // const totalPrice = totalIngredients.reduce((acc, p)=>{
    //     acc+=p.price
    // },0);
    let totalPrice = null;
    for (let i = 0; i < totalIngredients.length; i++) {
        totalPrice=totalPrice+totalIngredients[i];
    }

    console.log(totalPrice);

    const addCount = id=>{
        console.log('click',id);
        setIngredients(Ingredients.map(ingredient =>{
                if(ingredient.id === id){
                    return {
                        ...ingredient,
                        count: ingredient.count+1,
                    }
                }
                return ingredient;
        }))
    }

    const deleteCount = id=>{
        console.log('click',id);
        setIngredients(Ingredients.map(ingredient =>{
            if(ingredient.id === id){
                return {
                    ...ingredient,
                    count: ingredient.count-1,
                }
            }
            return ingredient;
        }))
    }


    const ingredientComponents= INGREDIENTS.map((ing,i)=>(
        <Ingredient
            key ={i}
            image={ing.image}
            name={ing.name}
            count = {Ingredients[i].count}
            id = {i}
            onAddCount = {()=>addCount(ing.id)}
            onDeleteCount = {()=>deleteCount(ing.id)}
        />
    ))

    return(
        <div className="App">
            <div className='boxes'>
                <div className='ingredients box'>
                    <h2>Ingredients</h2>
                    {ingredientComponents}
                </div>
                <div className='burger box'>
                    <h2>Burger</h2>
                    <p>Total price: {totalPrice}</p>
                </div>
            </div>
        </div>
    )
}

export default App;
